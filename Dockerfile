# Start from the code-server Debian base image
FROM gitpod/workspace-mysql

# Install code-server (VSCode Web)
RUN curl -fsSL https://code-server.dev/install.sh | sh
RUN mkdir -p .config/code-server
COPY config.yaml .config/code-server/config.yaml
RUN mkdir -p .local/share/code-server/User
COPY settings.json .local/share/code-server/User/settings.json
EXPOSE 8080

# Entrypoint to launch MySQL & Code-Server
COPY entrypoint.sh /usr/bin/entrypoint.sh
USER root
RUN chmod +x /usr/bin/entrypoint.sh
RUN mkdir /workspace
RUN chmod -R 777 /workspace
USER gitpod

ENTRYPOINT ["/usr/bin/entrypoint.sh"]
