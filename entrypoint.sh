#!/bin/bash

# Launch MySQL

if [ ! -e /var/run/mysqld/gitpod-init.lock ]
then
    touch /var/run/mysqld/gitpod-init.lock
    # initialize database structures on disk, if needed
    [ ! -d /workspace/mysql ] && mysqld --initialize-insecure
    # launch database, if not running
    [ ! -e /var/run/mysqld/mysqld.pid ] && mysqld --daemonize --verbose
    rm /var/run/mysqld/gitpod-init.lock
fi

# Get repository
git clone $GIT_REPO $START_DIR

# Launch Code-Server
code-server $START_DIR
